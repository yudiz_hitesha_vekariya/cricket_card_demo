import React from "react";
import axios from "axios";
import MainCard from "./components/card/MainCard";
import { useQuery } from "react-query";
import "./App.css";

function App() {
  function fetchData() {
    const data = axios.get("cricket.json");
    return data;
  }
  const {  data } = useQuery("fetch_data", fetchData);
  return (
    <div className="staring">
      <MainCard data={data?.data.data} />
    </div>
  );
}

export default App;
