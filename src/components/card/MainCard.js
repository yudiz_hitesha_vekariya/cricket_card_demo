import React from "react";
import PropTypes from "prop-types";
import moment from "moment";
import placeHolderLogoImage from "../../images/logo-placeholder.svg";
import Player from "../../images/player.png";
import winnerLogoImage from "../../images/winner-cup.svg";
import "./MainCard.css";

function Card({ data }) {
  const date = moment(data?.sport_event.scheduled).format("Do MMMM YYYY");

  return (
    <div className="main_div">
      <div className="title">
        <p className="ptitle">{data?.sport_event.season.name}</p>
        <h1>
          {data?.sport_event.competitors[0].name +
            " VS " +
            data?.sport_event.competitors[1].name}
        </h1>
      </div>
      <div className="description">
        <p className="address">
          {data?.sport_event.venue.name +
            ", " +
            data?.sport_event.venue.city_name +
            " | " +
            date}
        </p>
        <p className="message">{data?.sport_event_status.match_result}</p>
      </div>
      <div className="bottom_section">
        <div className="card_score">
          <div className="scores">
            <div className="win">
              <h4 className="winner">
                <img src={placeHolderLogoImage} alt="" className="placelogo" />

                {
                  data?.sport_event.competitors.find(
                    (item) => item.id != data?.sport_event_status.winner_id
                  ).abbreviation
                }
              </h4>
              <h4 className="score1">
                {data?.sport_event_status.period_scores[0].display_score +
                  " (" +
                  data?.sport_event_status.period_scores[0].display_overs +
                  " OV)"}
              </h4>
            </div>
            <div className="win">
              <h4 className="winner">
                <img src={placeHolderLogoImage} alt="" className="placelogo" />
                {
                  data?.sport_event.competitors.find(
                    (item) => item.id == data?.sport_event_status.winner_id
                  ).abbreviation
                }

                <img src={winnerLogoImage} alt="" className="winlogo" />
              </h4>
              <h4 className="score_win">
                {data?.sport_event_status.display_score +
                  " (" +
                  data?.sport_event_status.display_overs +
                  " OV)"}
              </h4>
            </div>
          </div>
        </div>
        <div className="player">
          <img className="player_img" src={Player} alt=" " />
          <div>
            <p>PLAYER OF THE MATCH</p>

            <p className="info">
              {data?.statistics.man_of_the_match[0].name
                .split(",")
                .reverse()
                .map((name) => name + " ")}
            </p>
          </div>
        </div>
      </div>
    </div>
  );
}

Card.propTypes = {
  data: PropTypes.object,
};
export default Card;
